const { router } = require("../server");
const noticiaCorta = require('../models/noticiacorta.model')
const indexCtrl = {};


 indexCtrl.renderIndex = async (req, res) => {
        noticiasCortas = await noticiaCorta.find();
         res.render('../views/index', { noticiasCortas})

}
indexCtrl.renderAbout = (req, res) => {
         res.render('about')
}
module.exports = indexCtrl;
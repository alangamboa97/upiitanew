const { userInfo } = require('os');
const noticiaLarga = require('../models/noticialarga.model')
const noticiaCorta = require('../models/noticiacorta.model')

const newsController = {};

newsController.listarNoticia = (req, res) =>{
         noticiaCorta
         .find()
         .then((data) => res.json(data))
         .catch((error) => res.json({mensaje : error}))
        

}

newsController.renderNoticias = async (req, res) => {

         noticiasCortas = await noticiaCorta.find();
         res.render('../views/index', { noticiasCortas})
}


     
newsController.crearNoticiaLarga = (req,res) => {
         
        
         const noticia = noticiaLarga(req.body);
         noticia
         .save()
         .then((data) => res.json(data))
         .catch((error) => res.json({mensaje : error}))
         
        
}
newsController.crearNoticiaCorta = (req,res) => {
         
        
         const noticia = noticiaCorta(req.body);
         noticia
         .save()
         .then((data) => res.json(data))
         .catch((error) => res.json({mensaje : error}))
         
        
}
newsController.eliminarNoticia =(req,res) => {
         res.send('borrar noticia')
}




module.exports = newsController;
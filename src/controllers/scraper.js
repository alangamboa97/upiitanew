const axios = require('axios')
const cheerio = require('cheerio')
const { response } = require('../server')

const url = 'https://news.mit.edu/topic/artificial-intelligence2'

axios(url, { Headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36 Edg/100.0.1185.29'}}
).then(response =>{
         const html = response.data;
         const $ = cheerio.load(html);
         const noticiaInfo = [
                  {
                           titulo: $('h3').children().text().trim(),
                           descripcion:$('p').children().text().trim()
                          


                  }
         ]
         console.log(noticiaInfo[0])
})

module.exports = noticiaInfo;
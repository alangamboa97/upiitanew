const {Router} = require('express')
const router = Router();
const {noticiacortaModel} = require('../models/noticiacorta.model')

const {renderIndex, renderAbout} = require('../controllers/index.controllers')


router.get('/', renderIndex )
router.get('/about', renderAbout)

module.exports = router;
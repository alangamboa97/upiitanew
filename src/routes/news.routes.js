const {Router} = require('express');
const newsController = require('../controllers/news.controller');
const router = Router()

const { crearNoticia } = require('../controllers/news.controller');

router.get('/news', newsController.renderNoticias)
router.post('/news/add',newsController.crearNoticiaLarga)
router.post('/news/adds',newsController.crearNoticiaCorta)
router.delete('/news/delete/:id', newsController.eliminarNoticia)

module.exports = router
const {Schema, model} = require('mongoose');



        
const noticiaCorta = new Schema({
         titulo: {
             type: String,
             required: true
         },
     
         descripcion: {
             type: String,
             required: false
         },
     
        
         fecha: {
             type: String,
             required: false
            
         },
         imagen:{
            type: String,
            required: false
         },
         autor: {
             type: String,
             required: false
         },

         url:{
             type: String,
             required: false
         }


});

module.exports = model('noticiaCorta', noticiaCorta);


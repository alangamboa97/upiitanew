const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const cheerio = require("cheerio");
//Inicializadores
const app = express();

//Configuracion
app.set('port', process.env.PORT || 1023);
app.set('views',path.join (__dirname , 'views'));
app.engine('.hbs', exphbs.engine({
         defaultLayout: 'main',
         layoutsDir: path.join(app.get('views'),'layouts'),
         partialsDir: path.join(app.get('views'),'partials'),
         extname: '.hbs',
         runtimeOptions: {
                  allowProtoPropertiesByDefault: true,
                  allowProtoMethodsByDefault: true,
              },
}));
app.set('view engine', '.hbs');
//Middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json())

//Variables Globales

//Rutas
app.use(require('./routes/index.routes'));
app.use(require('./routes/news.routes'));
//static files
app.use(express.static(path.join(__dirname, 'public')));





module.exports = app;
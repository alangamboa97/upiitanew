from gc import collect
from bs4 import BeautifulSoup

from pymongo import MongoClient

import requests
import pandas as pd

client = MongoClient('mongodb+srv://root:2104782@cluster0.zrhyx.mongodb.net/News2?retryWrites=true&w=majority')
url = 'https://www.xataka.com/tag/inteligencia-artificial'
page = requests.get(url)
soup = BeautifulSoup(page.text,'html.parser')


db = client['News2']
collection = db['noticiacortas']
print(db.list_collection_names())

#Titulares
titulares_list = list()
for div in soup.find_all('h2', class_='abstract-title'):
    for p in div.find_all('a'):
        titulares_list.append(p.text)
        print(p.text)
print('Titulos:', len(titulares_list))
        
    #else:
     
     #   break
    #count +=1
#print(titulares_list)

#descripcion


count = 0
chisp_list = list()
for div in soup.find_all('div', class_='abstract-excerpt'):
    for p in div.find_all('p'):
        chisp_list.append(p.text)
        print(p.text)
print('Descripciones: ', len(chisp_list))

   

#print(chisp_list)

#fecha
fecha_list = list()
for div in soup.find_all('footer', class_='abstract-byline'):
    for p in div.find_all('time'):
        fecha_list.append(p.text)
        print(p.text)
print('fechas:', len(fecha_list))


#imagenes
imagen_list = list()
for div in soup.find_all('div', class_='base-asset-image'):
    for p in div.find_all('img'):
        print(p['src'])
        
        imagen_list.append(p['src'])
        
print('Imagenes:', len(imagen_list))

#urls
url_list = list()
for div in soup.find_all('h2', class_='abstract-title'):
    for p in div.find_all('a'):
        url_list.append(p['href'])
        print(p['href'])

print('Url:', len(url_list))

#autores
autores_list = list()
for div in soup.find_all('a', class_='abstract-author'):
    
        autores_list.append(div.text)
        print(div.text)

print('Autores:', len(autores_list))


#df = pd.DataFrame({'Titular':titulares_list, 'Check':chisp_list}, index=list(range(1,16)))
#print(titulares_list)
#print(chisp_list)
i=0

collection.delete_many({})

#agregar Noticias Cortas


while i < len(titulares_list):
    collection.insert_one({"titulo": titulares_list[i],
                            "descripcion": chisp_list[i],
                            "fecha": fecha_list[i],
                            "imagen" : imagen_list[i],
                            "url": url_list[i],
                            "autor": autores_list[i]

                            })
    
    i = i+1



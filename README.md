# UpiiNews webscrapping API


- Gamboa Del Ángel Alan Eduardo
- Díaz Martínez Maite Paulette
- Sánchez Rodríguez Mirza Samantha
- Gonzalez Ayala Marcos Andres



# Backend

 - MongoDB
 - NodeJs
 - PyMongo
 - BeautifulSoup
 - ExpressJs
 - Conda




# FrontEnd
- HandleBars
- Bootstrap


# Dependencias a instalar

Para instalar todas las dependencias necesarias ejecute el siguiente comando en el directorio raiz:


- pip install -r requirements.txt


## Diagrama

https://photos.app.goo.gl/o32wGu1PiHNomdyn9
